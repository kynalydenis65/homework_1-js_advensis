class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    get age() {
        return this._age;
    }

    set age(age) {
        this._age = age
    }

    get salary() {
        return this._salary;
    }

    set salary(salary) {
        this._salary = salary
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }

    get salary() {
        return super.salary * 3
    }
}

let programmer1 = new Programmer('Denys', 32, 60000, ['JavaScript', 'Python']);
let programmer2 = new Programmer('Marck', 20, 10000, ['Java', 'JavaScript']);

console.log(programmer1);
console.log(programmer2);